from backend import mongo
from bson.json_util import dumps
from json import loads


queryset = loads(dumps(mongo.db.users.find()))

with open('bot/data/birthday.json', 'w') as f:
    f.writelines(str(queryset))

queryset = loads(dumps(mongo.db.payments.find()))

with open('bot/data/payments.json') as f:
    f.writelines(str(queryset))
