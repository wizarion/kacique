const TelegramBot = require('node-telegram-bot-api');
const token = require('./token');


const bot = new TelegramBot(token['token'], {polling: true});

bot.onText(/\/ajuda/, (msg) => {
    const chatId = msg.chat.id;
    var resp = `Opções: \n
        /ajuda \n
        /aniversario \n
        /eventos \n
        /financeiro \n
        /reuniao \n
        /social`
    bot.sendMessage(chatId, resp);
});

bot.onText(/\/aniversario/, (msg) => {
    const chatId = msg.chat.id;
    var resp = `Aniversariante(s) do mês: \n 
        X \n
        Y \n
        Z`
    bot.sendMessage(chatId, resp);
});

bot.onText(/\/eventos/, (msg) => {
    const chatId = msg.chat.id;
    var resp = `Evento(s) do mês: \n
        XX/XX - Descrição \n
        YY/YY  - Descrição \n
        ZZ/ZZ  - Descrição \n
        Acesse http://kaetemoto.club/pages/agenda.html`
    bot.sendMessage(chatId, resp);
});

bot.onText(/\/financeiro/, (msg) => {
    const chatId = msg.chat.id;
    var resp = `Caixa total: R$ Z \n
        Entrada(s): R$ X  \n
        Saída(s): R$ Y`
    bot.sendMessage(chatId, resp);
});

bot.onText(/\/reuniao/, (msg) => {
    const chatId = msg.chat.id;
    var resp = `Reunião Oficial será sempre no último sábado do mês. \n
        Acesse http://kaetemoto.club/pages/agenda.html`
    bot.sendMessage(chatId, resp);
});

bot.onText(/\/social/, (msg) => {
    const chatId = msg.chat.id;
    var resp = `Ação Social: \n
        XX/XX  - Descrição \n
        YY/YY  - Descrição \n
        ZZ/ZZ  - Descrição \n 
        Acesse http://kaetemoto.club/pages/agenda.html`
    bot.sendMessage(chatId, resp);
});
