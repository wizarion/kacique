from flask import Flask
from flask_pymongo import PyMongo
from flask_cors import CORS
from flask_jwt_extended import JWTManager


app = Flask(__name__)
app.config.from_object('backend.config.DevelopmentConfig')
CORS(app)
mongo = PyMongo(app)
jwt = JWTManager(app)

from . import auth
app.register_blueprint(auth.bp)

from . import users
app.register_blueprint(users.bp)

from . import motorcycles
app.register_blueprint(motorcycles.bp)

from . import finances
app.register_blueprint(finances.bp)
