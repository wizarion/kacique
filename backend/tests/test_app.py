import os
import pytest
from backend import app, mongo
from flask_pymongo import PyMongo

app.config.from_object('backend.config.TestingConfig')
mongo = PyMongo(app)

@pytest.fixture
def client():
    pass
