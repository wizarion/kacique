from flask import Blueprint, jsonify, request, Response
from werkzeug.security import generate_password_hash, \
    check_password_hash
from bson.json_util import dumps
from bson.objectid import ObjectId
from json import loads
from flask_jwt_extended import jwt_required, create_access_token, \
    get_jwt_identity
from . import mongo, app, jwt
from datetime import datetime, timedelta

bp = Blueprint('auth', __name__, url_prefix='/api/auth')

@bp.route('/login', methods=['POST'])
def login():
    '''Login method.'''
    
    if request.method == 'POST':
        if not request.is_json:
            return jsonify({ 'message': 'JSON requerido!' }), 400

        data = request.get_json()

        if not data['username'] or not data['password']:
            return jsonify({ 'message': 'Dados incorretos!' }), 400

        user = mongo.db.users.find_one({'username': data['username']})
        
        if user and check_password_hash(user['password'], data['password']):
            if user['status'] == 'active' or user['status'] == True :
                login_expire = timedelta(days=1)
                token = create_access_token(
                    identity = data['username'],
                    expires_delta = login_expire
                )
                user['token'] = token
                mongo.db.users.update_one(
                    { '_id': ObjectId(user['_id']) },
                    { '$set': user }, upsert=False)
                return jsonify(
                    { 
                        '_id': user['id']['$oid'],
                        'username': user['username'],
                        'token': user['token'],
                        'admin': user['admin'] 
                    }
                ), 200
            else:
                return jsonify({ 'message': 'Não autorizado!' }), 401
        else:
            return jsonify({ 'message': 'Dados incorretos!' }), 401
