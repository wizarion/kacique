import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AuthGuardService } from "../services/guard-service/auth-guard.service";

import { FinancesComponent } from "./finances.component";
import { FinancesFormComponent } from "./finances-form/finances-form.component";


const financesRoutes = [
    { 
        path: 'finances',
        component: FinancesComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'finances/add',
        component: FinancesFormComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'finances/edit/:id',
        component: FinancesFormComponent,
        canActivate: [AuthGuardService]
    }
];

@NgModule({
    imports: [RouterModule.forChild(financesRoutes)],
    exports: [RouterModule]
})

export class FinancesRoutingModule {}
