import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FinanceService } from '../../services/finance-service/finance.service'; 
import { Finance } from '../../core/models/finance.model';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { SharedService } from 'src/app/services/shared-service/shared.service';
import { MemberService } from '../../services/member-service/member.service';
import { Member } from '../../core/models/member.model';
@Component({
  selector: 'app-finances-form',
  templateUrl: './finances-form.component.html',
  styleUrls: ['./finances-form.component.css']
})
export class FinancesFormComponent implements OnInit {
  errorMessage = '';
  financeForm: FormGroup;
  hasData: any;
  loading: boolean = false;
  disabled: boolean = false;
  names: any;
  memberList: [];
  namesList = [
    {
      'key': 'Patches',
      'value': 'Patches'
    },
    {
      'key': 'Adesivos',
      'value': 'Adesivos'
    },
    {
      'key': 'Confraternização',
      'value': 'Confraternização'
    },
    {
      'key': 'Ajuda',
      'value': 'Ajuda'
    },
  ];
  
  constructor(
    private financeService: FinanceService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private memberService: MemberService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.loadMember();
  }

  initForm() {
    const id = this.route.snapshot.params['id'];

    if (this.router.getCurrentNavigation()?.extras.state) {
      this.hasData = this.router.getCurrentNavigation()?.extras.state;
    }

    if (!this.hasData && id) {
      this.financeService.getOne(id).subscribe(
        data  => {
          this.hasData = data;
          this.loadForm();
        }
      );
      return;
    }

    this.loadForm();
  }

  loadMember(): void {
    this.memberService.getAll().subscribe(
      data => {
        this.memberList = data.map(
          (member: Member) => {
            return {
              'key': member.username,
              'value': `${member.firstname} ${member.lastname}`
            }
          }
        );
      }
    );
  }

  loadForm(): void {
    this.financeForm = this.formBuilder.group({
      id: [
        this.hasData ? this.hasData._id: null
      ],
      category: [
        this.hasData ? this.hasData.category : null,
        [ Validators.required ]
      ],
      name: [
        this.hasData ? this.hasData.name : null,
        [ 
          Validators.required,
          Validators.minLength(4)
        ]
      ],
      operation: [
        this.hasData ? this.hasData.operation : null,
        [ Validators.required ]
      ],
      method: [
        this.hasData ? this.hasData.method : null,
        [ Validators.required ]
      ],
      date: [
        this.hasData ? this.hasData.date : null,
        [ Validators.required ]
      ],
      value: [
        this.hasData ? this.hasData.value : null,
        [ Validators.required  ]
      ],
      description: [
        this.hasData ? this.hasData.value : null,
        [ Validators.required ]
      ]
    });
  }

  getErrorMessage(field: string) {
    let controls = this.financeForm.controls;
    
    if (controls[field].hasError('required')) return 'Campo obrigatório';
    return `Mínimo de ${controls[field].errors?.minlength?.requiredLength} caracteres`;
  }

  onSetName(value: string): void {
    value === 'Mensalidade' ? this.names = this.memberList : this.names = this.namesList;
  }

  onSubmit(): void {
    if (this.financeForm.valid) {
      if (!this.hasData) {
        this.financeService.post(this.financeForm.value).subscribe(
          () => {
            this.router.navigate(['finances']);
            this.sharedService.showMessage('Finança cadastrada com sucesso!');
          },
          (err: any) => {
            this.sharedService.showMessage('Erro ao cadastrar finança!');
            this.errorMessage = err.error.message;
          }
        );
      } else {
        this.financeService.put(this.financeForm.value).subscribe(
          () => {
            this.router.navigate(['finances']);
            this.sharedService.showMessage('Finança atualizada com sucesso!');
          },
          (err: any) => {
            this.sharedService.showMessage('Erro ao atualizar finança!');
            this.errorMessage = err.error.message;
          }
        );
      }
    }
  }

  onCancel(): void {
    this.router.navigate(['finances']);
  }
}
