import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { ProfileComponent } from "./profile.component";

@NgModule({
    declarations: [
        ProfileComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [],
    providers: []
})

export class ProfileModule {}