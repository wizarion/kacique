import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { Member } from '../core/models/member.model';
import { MemberService } from '../services/member-service/member.service';
import { SharedService } from '../services/shared-service/shared.service';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  public info: any[] = [{
      title: 'Nome',
      model: 'firstname',
      showOnTable: true
    }, {
      title: 'Sobrenome',
      model: 'lastname',
      showOnTable: true
    }, {
      title: 'Sexo',
      model: 'genre',
      showOnTable: true,
      customFormat: (genre: string) => {
        let types = <any> {
          male: 'Masculino',
          female: 'Feminino'
        }
        return types[genre];
      }
    }, {
      title: 'Idade',
      model: 'birth',
      showOnTable: true,
      customFormat: (birth: Date) => {
        birth = new Date(birth);
        const dataAtual = new Date();
        return `${birth.toLocaleDateString('pt-BR')} 
        (${dataAtual.getFullYear() - birth.getFullYear()} anos)`
      }
    },  {
      title: 'Celular',
      model: 'phone',
      customFormat: (phone: string) => {
        return ['(', ...phone.slice(0, 2), ') ', 
          ...phone.slice(2, 7), '-', 
          ...phone.slice(7)].join('');
      }
    }, {
      title: 'Tipo Sanguíneo',
      model: 'bloodType',
      showOnTable: true,
      customFormat: (bloodType: string) => {
        let convert = <any> {
          aPlus: 'A+',
          aMinus: 'A-',
          bPlus: 'B+',
          bMinus: 'B-',
          abPlus: 'AB+',
          abMinus: 'AB-',
          oPlus: 'O+',
          oMinus: 'O-'
        }
        return convert[bloodType];
      }
    }, {
      title: 'Estado civil',
      model: 'civil',
      customFormat: (civil: string) => {
        let convert = <any> {
          single: 'Solteiro(a)',
          married: 'Casado(a)',
          separate: 'Separado(a)',
          divorced: 'Divorciado(a)',
          widower: 'Viúvo(a)'
        }
        return convert[civil];
      }
    }, {
      title: 'Ocupação',
      model: 'occupation'
    }, {
      title: 'Carteira de habilitação',
      model: 'driverLicense'
    }, {
      title: 'Data de emissão',
      model: 'emissionDate',
      customFormat: (emissionDate: Date) => 
        new Date(emissionDate).toLocaleDateString('pt-BR')
    }, {
      title: 'Data de validade',
      model: 'expirationDate',
      customFormat: (expirationDate: Date) => 
        new Date(expirationDate).toLocaleDateString('pt-BR')
    }, {
      title: 'PIX',
      model: 'pix',
      showOnTable: true
    }, {
      title: 'CEP',
      model: 'cep',
      customFormat: (cep: string) => {
        return [...cep.slice(0, 5), '-', 
        ...cep.slice(5)].join('');
      }
    }, {
      title: 'Endereço',
      model: 'address'
    }, {
      title: 'Número',
      model: 'number'
    }, {
      title: 'Bairro',
      model: 'neighborhood'
    }, {
      title: 'Estado',
      model: 'state'
    }, {
      title: 'Cidade',
      model: 'city'
    }, {
      title: 'Email',
      model: 'email'
    }, {
      title: 'Naturalidade',
      model: 'naturalness'
    }, {
      title: 'Nacionalidade',
      model: 'nationality'
    }, {
      title: 'Contato de emergência',
      model: 'contact'
    }, {
      title: 'Padrinho',
      model: 'godfather'
    }, {
      title: 'Data de Batismo',
      model: 'baptismDate',
      showOnTable: true,
      customFormat: (baptismDate: Date) => 
        new Date(baptismDate).toLocaleDateString('pt-BR')
    }, {
      title: 'Data de Filiação',
      model: 'affiliationDate',
      showOnTable: true,
      customFormat: (affiliationDate: Date) => 
        new Date(affiliationDate).toLocaleDateString('pt-BR')
    }, {
      title: 'Status',
      model: 'status',
      showOnTable: true,
      customFormat: (status: string) => {
        let convert = <any> {
          active: 'Ativo',
          onLeave: 'Licença',
          freePass: 'Isento',
          punishment: 'Afastado',
          excommunicado: 'Desligado'
        }
        return convert[status];
      }
    }, {
      title: 'Doador de órgãos',
      model: 'donor',
      customFormat: (donor: boolean) => donor ? 'Sim' : 'Não'
    }
  ];

  public dataSource: MatTableDataSource<Member>;
  public dataObserv: Observable<any>;
  public errorMessage = '';

  constructor(
    private MemberService: MemberService,
    private sharedService: SharedService
  ) {}

  ngOnInit(): void {
    this.updateDataSource();
  }

  updateDataSource() {
    this.MemberService
    .getAll()
    .subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data);
    }, err => {
      this.sharedService.showMessage('Erro na busca dos dados');
      this.errorMessage = err.error.message;
    });
  }

  deleteItem(id: any): void {
    this.MemberService
    .delete(id)
    .subscribe(() => {
      this.updateDataSource();
      this.sharedService.showMessage('Usuário removido com sucesso!');
    })
  }
}