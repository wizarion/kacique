import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AuthGuardService } from "../services/guard-service/auth-guard.service";

import { MemberComponent } from "./member.component";
import { MemberFormComponent } from "./member-form/member-form.component";


const memberRoutes = [
    { 
        path: 'member', 
        component: MemberComponent,
        canActivate: [AuthGuardService] 
    }, {
        path: 'member/add',
        component: MemberFormComponent,
        canActivate: [AuthGuardService]
    }, {
        path: 'member/edit/:id',
        component: MemberFormComponent,
        canActivate: [AuthGuardService]
    }
];

@NgModule({
    imports: [RouterModule.forChild(memberRoutes)],
    exports: [RouterModule]
})

export class MemberRoutingModule {}