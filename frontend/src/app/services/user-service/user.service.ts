import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { PATH } from '../../../environments/environment';

const API = `${PATH}/users/`;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUser(): Observable<any> {
    return this.http.get(API + 'get', { responseType: 'text' });
  }

  getAdmin(): Observable<any> {
    return this.http.get(API + 'admin', { responseType: 'text' });
  }
}
