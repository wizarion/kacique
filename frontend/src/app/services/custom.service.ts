import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomService {
  constructor(private snackBar: MatSnackBar) {}
  showMessage(msg: string, isError: boolean = false): void {
    this.snackBar.open(msg, 'X', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: isError ? ['msg-error'] : ['msg-success'],
    });
  }

  errorHandler(e: any): Observable<any> {
    console.log('Erro: ', e);
    this.showMessage('Ocorreu um erro!', true);
    return EMPTY;
  }
}
