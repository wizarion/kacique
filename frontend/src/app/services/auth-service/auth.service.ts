import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PATH } from '../../../environments/environment';
import { TokenService } from '../token-service/token.service';

const AUTH_API = `${PATH}/auth/`;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public logged: boolean;

  constructor(
    private http: HttpClient,
    private tokenService: TokenService
  ) {
    if(tokenService.getToken()) {
      this.logged = true;
    }
  }

  get isLogged(): boolean {
    return this.logged;
  }

  set isLogged(val: boolean) {
    this.logged = val;
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'login', {
      username,
      password
    }, httpOptions);
  }
}