import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PATH } from '../../../environments/environment';
import { Member } from 'src/app/core/models/member.model';
import { TokenService } from '../token-service/token.service';

const API = `${PATH}/users/`;

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  private token: any;
  private header: HttpHeaders;

  constructor(
    private http: HttpClient,
    private tokenService: TokenService,
  ) {
    this.token = this.tokenService.getUser();
    this.header = new HttpHeaders({ 
      Authorization: `Bearer ${ this.token['token'] }`
    });
  }

  getAll(): Observable<any> {
    return this.http.get(
      API + 'get',
      { headers: this.header }
    );
  }

  getOne(id: string): Observable<any> {
    return this.http.get(
      `${API}get/${id}`,
      { headers: this.header }
    );
  }

  getByName(username: string): Observable<any> {
    return this.http.get(
      `${API}get-by-username/${username}`,
      { headers: this.header }
    );
  }

  post(member: Member): Observable<any> {
    return this.http.post<any>(
      API + 'post',
      member,
      { headers: this.header }
    );
  }

  put(id: string, member: Member): Observable<any> {
    return this.http.put(
      API + `put/${id}`,
      member, 
      { headers: this.header }
    );
  }

  delete(member: Member): Observable<any> {
    return this.http.delete(
      API + `delete/${ member }`,
      { headers: this.header }
    );
  }
}