import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth-service/auth.service';
import { SharedService } from '../services/shared-service/shared.service';
import { TokenService } from '../services/token-service/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoginFailed = false;
  errorMessage = '';
  hide = true;

  constructor(
    private authService: AuthService, 
    private tokenService: TokenService,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.authService.isLogged = true;
    }
  }

  isLogged(): boolean {
    return this.authService.isLogged;
  }

  onSubmit(): void {
    const { username, password } = this.form;

    this.authService.login(username, password).subscribe(
      data => {
        this.tokenService.saveToken(data.token);
        this.tokenService.saveUser(data);
        this.isLoginFailed = false;
        this.authService.isLogged = true;
        this.sharedService.showMessage(`Bem-vindo ${data.username}!`)
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage(): void {
    window.location.reload();
  }
}
