import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogComponent } from '../dialog-component/dialog.component';
import { MemberService } from 'src/app/services/member-service/member.service';
import { MotorcycleService } from 'src/app/services/motorcycle-service/motorcycle.service';
import { FinanceService } from 'src/app/services/finance-service/finance.service';

@Component({
	selector: 'app-custom-table',
	templateUrl: './custom-table.component.html',
	styleUrls: ['./custom-table.component.css'],
	// animations: [
	// 	trigger('actions', [
	// 		state('collapsed', style({height: '0px', minHeight: '0'})),
	// 		state('expanded', style({height: '*'})),
	// 		transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
	// 	]),
	// ],
})
export class CustomTableComponent implements OnInit, AfterViewInit {

	@Input() public title: string;
	@Input() public icon: string;
	@Input() public tableInfo: any[];
	@Input() public dataSource: any;
	@Output() public onDelete = new EventEmitter<any>();
	
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	
	public displayedColumns: any[];
	
	constructor(
		private router: Router,
		private dialog: MatDialog,
		private memberService: MemberService,
		private motoService: MotorcycleService,
		private financeService: FinanceService
	) { }

	ngOnInit(): void {
		this.displayedColumns = this.getColumns(this.tableInfo);
	}
	
	ngAfterViewInit() {
		if (!this.dataSource) return;
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	getColumns = (data: any[]): string[] => {
		return [
			...data.filter(val => val.showOnTable && val.model).map(val => val.model), 
			'actions'
		];
	}

	applyFilter(event: Event): void {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	clickAction(action: string, element?: any): any {
		const typesRule = <any> {
			add: () => this.router.navigate([this.router.url, 'add']),
			view: () => this.router.navigate([this.router.url, 'view']),
			edit: () => this.router.navigate([
				this.router.url, 
				'edit', 
				element._id.$oid
			], { state: element }),
			remove: () => this.onDelete.emit(element)
		};
		return typesRule[action] ? typesRule[action]() : typesRule.add();
	}

	openViewDialog(id: string) {
		const config = new MatDialogConfig();
		this.getData(id).subscribe((data: any) => {
			config.autoFocus = true;
			config.data = {
				title: this.title,
				itemData: data,
				tableInfo: this.tableInfo
			}
			this.dialog.open(DialogComponent, config);
		});
	}

	getData(id: string) {
		const module = this.router.url.split('/')[1];
		const type = <any> {
			member: (id: string) => this.memberService.getOne(id),
			motorcycle: (id: string) => this.motoService.getOne(id),
			finances: (id: string) => this.financeService.getOne(id)
		}
		return type[module](id);
	}

	openRemoveDialog(template: TemplateRef<any>, id: string) {
		let config = {
			height: '170px',
			width: '300px'
		};
		let dialogRef = this.dialog.open(template, config);

		dialogRef.afterClosed().subscribe(confirm => {
			confirm && this.clickAction('remove', id);
		});
	}
}