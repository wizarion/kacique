import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
	selector: 'app-dialog',
	templateUrl: './dialog.component.html',
	styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

	public breakpoint: number;

	constructor(
		public dialogRef: MatDialogRef<DialogComponent>,
    	@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit(): void {
		this.onResize();
	}

	onResize() {
		this.breakpoint = 1;
	
		if (window.innerWidth > 600 && window.innerWidth < 845) {
		  this.breakpoint = 2;
		  return;
		}
		if (window.innerWidth >= 845 && window.innerWidth <= 1024) {
		  this.breakpoint = 3;
		  return;
		}
		if (window.innerWidth > 1024) {
		  this.breakpoint = 4;
		  return;
		}
	  }
}