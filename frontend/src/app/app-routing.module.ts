import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardService } from './services/guard-service/auth-guard.service';

import { MemberRoutingModule } from './member/member-routing.module';
import { FinancesRoutingModule } from './finances/finances-routing.module';
import { MotorcycleRoutingModule } from './motorcycle/motorcycle-routing.module';

const routes: Routes = [
  { 
    path: 'login', 
    component: LoginComponent 
  }, { 
    path: 'profile',
    component: ProfileComponent, 
    canActivate: [AuthGuardService]
  }, { 
    path: '', 
    redirectTo: 'login', 
    pathMatch: 'full' 
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    MemberRoutingModule,
    FinancesRoutingModule,
    MotorcycleRoutingModule
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}