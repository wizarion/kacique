import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from '../services/guard-service/auth-guard.service';

import { MotorcycleComponent } from './motorcycle.component';
import { MotorcycleFormComponent } from './motorcycle-form/motorcycle-form.component';

const motorcycleRoutes = [
  {
    path: 'motorcycle',
    component: MotorcycleComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'motorcycle/add',
    component: MotorcycleFormComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'motorcycle/edit/:id',
    component: MotorcycleFormComponent,
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(motorcycleRoutes)],
  exports: [RouterModule],
})
export class MotorcycleRoutingModule {}
